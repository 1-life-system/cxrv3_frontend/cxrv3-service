import Vue from "vue";
import VueRouter from "vue-router";
import Navbar from "@/components/Navbar.vue";
import Upload from "@/views/children/Upload.vue";
import History from "@/views/children/History.vue";
import Dashboard from "@/views/children/Dashboard.vue";
import Display from "@/views/children/Display.vue";
import Print from "@/views/children/Print.vue";
import User_guide from "@/views/children/User_guide.vue";
import DashboardHospitals from "@/views/children/DashboardHospitals.vue";
import Month from "@/components/dashboard/Month.vue";
import Year from "@/components/dashboard/Year.vue";
import VueCookies from "vue-cookies";

Vue.use(VueRouter);

const routes = [
  {
    path: "/dashboard/hospital",
    name: "DashboardHospitals",
    meta: { user: true },
    component: DashboardHospitals
  },
  {
    path: "/print",
    name: "Print",
    meta: { user: true },
    component: Print
  },
  {
    path: "/navbar",
    name: "Navbar",
    component: Navbar,
    meta: { viewer: true },
    children: [
      {
        path: "/upload",
        name: "Upload",
        meta: { user: true },
        component: Upload
      },
      {
        path: "/history",
        name: "History",
        meta: { viewer: true },
        component: History
      },
      {
        path: "/user_guide",
        name: "User_guide",
        meta: { user: true },
        component: User_guide
      },
      {
        path: "/display",
        name: "Display",
        meta: { user: true },
        component: Display
      },
      {
        path: "/",
        name: "Dashboard",
        meta: { user: true },
        component: Dashboard,
        redirect: "/month",
        children: [
          {
            path: "/month",
            name: "Month",
            component: Month,
            meta: { user: true }
          },
          {
            path: "/year",
            name: "Year",
            component: Year,
            meta: { user: true }
          }
        ]
      }
    ]
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.viewer)) {
    const checkcookie = VueCookies.get("service").package_use;
    console.log("check-cookies", checkcookie);
    const FindCxr = checkcookie.find(element => element == "cxr");
    if (!FindCxr) {
      const api = process.env.VUE_APP_API;
      console.log("api-router", api);
      window.location.href = api;
    } else if (to.matched.some(record => record.meta.user)) {
      const checkcookie = VueCookies.get("service");
      const FindCxr = checkcookie.package_use.find(element => element == "cxr");
      if (FindCxr) {
        if (checkcookie.data.role === "user") {
          next();
        } else {
          next({ name: "History" });
        }
      }
    } else {
      next();
    }
  } else {
    console.log("else");
    next();
  }
});

export default router;
