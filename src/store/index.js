import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    disease: [],
    upload_id: [],
    data_print: [],
  },
  mutations: {
    setDisease(state, data) {
      state.disease = data
    },
    setUpload_id(state, data) {
      state.upload_id = data
    },
    setData_print(state, data) {
      state.data_print = data
    }

  },
  actions: {},
  modules: {},
});
